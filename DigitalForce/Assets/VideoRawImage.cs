﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoRawImage : MonoBehaviour
{
    public RawImage _rawImage;
    public VideoPlayer _videoPlayer;
    public AudioSource _audioSource;

    private void Start()
    {
        StartCoroutine(PlayVideo());
    }

    private IEnumerator PlayVideo()
    {
        _videoPlayer.Prepare();

        while (!_videoPlayer.isPrepared)
        {
            yield return null;
        }

        _rawImage.texture = _videoPlayer.texture;
        _audioSource.Play();
    }
}