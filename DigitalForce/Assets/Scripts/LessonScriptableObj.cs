﻿using UnityEngine;
using UnityEngine.Video;

[CreateAssetMenu(menuName = "Lesson")]
public class LessonScriptableObj : ScriptableObject
{
    public GameObject _modelPrefab;
    public VideoClip _videoClip;
    public AudioClip _audioClip;
}