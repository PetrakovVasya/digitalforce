﻿using UnityEngine;
using UnityEngine.UI;

public class MainPageController : MonoBehaviour
{
    #region Serialized Fields

    [SerializeField] private Button _quitButton;
    [SerializeField] private Button _questionButton;

    #endregion

    #region Methods

    #region Unity Methods

    private void Awake()
    {
        _quitButton.onClick.AddListener(() => { Application.Quit(); });
    }

    #endregion

    #endregion
}