﻿using UnityEngine;
using UnityEngine.Video;

public class ImageTrackerContainer : DefaultTrackableEventHandler
{
    #region Serialized Fields

    [SerializeField] private VideoPlayer _videoPlayer;
    [SerializeField] private LessonScriptableObj _lesson;
    [SerializeField] private AudioSource _audioSource;

    #endregion

    #region Private Fields

    private Transform _cachedTransform;
    private GameObject _instantiatedModelPrefab;

    #endregion

    #region Methods

    #region Unity Methods

    private void Awake()
    {
        _cachedTransform = transform;
    }

    #endregion

    #region Override Methods

    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();

        HandleLessonScriptableObjOnTrackingFound();
    }

    protected override void OnTrackingLost()
    {
        base.OnTrackingLost();

        HandleLessonScriptableObjOnTrackingLost();
    }

    #endregion

    #region Private Methods

    private void HandleLessonScriptableObjOnTrackingFound()
    {
        if (_lesson._modelPrefab != null)
        {
            if (_instantiatedModelPrefab == null)
            {
                _instantiatedModelPrefab = Instantiate(_lesson._modelPrefab, _cachedTransform);
            }
            else
            {
                _instantiatedModelPrefab.gameObject.SetActive(true);
            }
        }

        if (_lesson._videoClip != null)
        {
            _videoPlayer.gameObject.SetActive(true);
            _videoPlayer.clip = _lesson._videoClip;
            _videoPlayer.Play();
        }
        else
        {
            _videoPlayer.gameObject.SetActive(false);
            if (_lesson._audioClip != null)
            {
                _audioSource.clip = _lesson._audioClip;
                _audioSource.Play();
            }
        }
    }

    private void HandleLessonScriptableObjOnTrackingLost()
    {
        _instantiatedModelPrefab?.SetActive(false);
        _videoPlayer.Pause();
        _audioSource.Pause();
    }

    #endregion

    #endregion
}